# Realworld Project

1. [Introducing realworld](https://medium.com/@ericsimons/introducing-realworld-6016654d36b5)
2. [Realworld on gihub](https://github.com/gothinkster/realworld)
3. [Realworld documentation and spec](https://realworld-docs.netlify.app/docs/intro)
4. [Demo projects on CodebaseShow](https://codebase.show/projects/realworld)

## Setup

### Setting Up a Virtual Environment
```
mkdir project_name
cd project_name
python3.8 -m venv venv
```

### Activate the Virtual Environment
```
source venv/bin/activate
```

### Dectivate the Virtual Environment
```
deactivate
```

### Required Files
**.env file**
```
export  SECRET_KEY=dev
export  JWT_SECRET_KEY='JWT_SECRET_KEY'
```

**.flaskenv**
```
export FLASK_ENV=development
export FLASK_APP=src
export SQLALCHEMY_DB_URI=sqlite:///realworld.db
```

## Help Command Examples
**create database using flask shell**
```
flask shell
from src.extensions import db
db.create_all()
db
quit()
```

**querying data**
```
sqlite3 src/realworld.db
.tables
.schema
.exit
```

**requirements file**
create requirements file
```
pip3 freeze > requirements.txt
```
install from requirements file
```
pip install -r requirements.txt
```

**flask-migrate**
create a migration repository with the following command:
```
flask db init
```
generate an initial migration files from dbmodels:
```
flask db migrate -m "Initial migration."
```
apply the migration to the database:
```
flask db upgrade
```

