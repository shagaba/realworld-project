from flask.json import jsonify
from .constants.http_status_codes import HTTP_500_INTERNAL_SERVER_ERROR


def json_error(message, status_code=HTTP_500_INTERNAL_SERVER_ERROR):
    return jsonify({'error': message}), status_code
