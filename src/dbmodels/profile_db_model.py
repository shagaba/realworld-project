from ..extensions import db
from datetime import datetime


class FollowAssociation(db.Model):
    followed_user_id = db.Column(db.Integer, db.ForeignKey('user.id'), primary_key=True)
    follower_user_id = db.Column(db.Integer, db.ForeignKey('user.id'), primary_key=True)
    created_at = db.Column(db.DateTime, default=datetime.now())
    updated_at = db.Column(db.DateTime, default=datetime.now())

    followed_user = db.relationship('User', foreign_keys=[followed_user_id])
    follower_user = db.relationship('User', foreign_keys=[follower_user_id])

    def __repr__(self):
        return f'<Follow {self.followed_user_id, self.follower_user_id}>'

    def as_dictionary(self):
        return {
            'followed_user_id': self.followed_user_id,
            'follower_user_id': self.follower_user_id,
            'created_at': self.created_at,
            'updated_at': self.updated_at
        }
