from ..extensions import db
from datetime import datetime


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.Text, nullable=False, unique=True)
    email = db.Column(db.Text, nullable=False, unique=True)
    password = db.Column(db.Text, nullable=False)
    bio = db.Column(db.Text, nullable=True)
    image = db.Column(db.Text, nullable=True)
    created_at = db.Column(db.DateTime, default=datetime.now())
    updated_at = db.Column(db.DateTime, default=datetime.now())

    # profile = db.relationship('DbProfile', back_populates='user', uselist=False)

    def __repr__(self):
        return f'<User {self.username}>'

    def as_auth_dictionary(self, token):
        return {
            'username': self.username,
            'email': self.email,
            'bio': self.bio,
            'image': self.image,
            'token': token,
        }
