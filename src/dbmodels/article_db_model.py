from ..extensions import db
from datetime import datetime

from ..services.profile_service import ProfileService


class Article(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    slug = db.Column(db.Text, unique=True, nullable=True)
    title = db.Column(db.Text, nullable=False)
    description = db.Column(db.Text, nullable=False)
    body = db.Column(db.Text, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    comments = db.relationship('Comment', backref='article')
    created_at = db.Column(db.DateTime, default=datetime.now())
    updated_at = db.Column(db.DateTime, default=datetime.now())

    author = db.relationship('User', foreign_keys=[user_id])

    def __repr__(self):
        return f'<Article {self.slug}>'

    def as_author_dictionary(self, follower_user_id):
        profile = ProfileService.get_followed_user_profile(self.author, follower_user_id)
        return {
            'id': self.id,
            'slug': self.slug,
            'title': self.title,
            'description': self.description,
            'body': self.body,
            'user_id': self.user_id,
            'comments': self.comments,
            'created_at': self.created_at,
            'updated_at': self.updated_at,
            'author': profile.as_dictionary()
        }

    def as_dictionary(self):
        return {
            'id': self.id,
            'slug': self.slug,
            'title': self.title,
            'description': self.description,
            'body': self.body,
            'user_id': self.user_id,
            'comments': self.comments,
            'created_at': self.created_at,
            'updated_at': self.updated_at
        }


class Comment(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.Text, nullable=False)
    article_id = db.Column(db.Integer, db.ForeignKey('article.id'), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    created_at = db.Column(db.DateTime, default=datetime.now())
    updated_at = db.Column(db.DateTime, onupdate=datetime.now())

    def __repr__(self):
        return f'<Comment {self.id}>'

    def as_dictionary(self):
        return {
            'id': self.id,
            'body': self.body,
            'article_id': self.article_id,
            'user_id': self.user_id,
            'created_at': self.created_at,
            'updated_at': self.updated_at
        }
