import json

from flask import Blueprint, request
from flask.json import jsonify
from flask_jwt_extended import jwt_required, get_jwt_identity
from marshmallow import ValidationError
from sqlalchemy.exc import IntegrityError
from ..constants.http_status_codes import HTTP_200_OK, HTTP_201_CREATED, HTTP_204_NO_CONTENT, HTTP_400_BAD_REQUEST, \
    HTTP_404_NOT_FOUND, HTTP_409_CONFLICT, HTTP_422_UNPROCESSABLE_ENTITY
from ..dto.article_schema import create_article_cmd_schema, update_article_cmd_schema
from ..errors import json_error
from ..dbmodels.article_db_model import Article
from ..services.article_service import ArticleService

articles_api = Blueprint('articles_api', __name__, url_prefix="/api/articles")


# create_article
@articles_api.post("/")
@jwt_required()
def create_article():
    user_id = get_jwt_identity()
    try:
        article_data = create_article_cmd_schema.loads(json_data=json.dumps(request.json))
    except ValidationError as err:
        return json_error(err.messages, HTTP_400_BAD_REQUEST)

    if ArticleService.get_article_by_slug(article_data['slug']):
        return json_error('Slug already exists', HTTP_409_CONFLICT)

    article = Article(slug=article_data['slug'],
                      title=article_data['title'],
                      description=article_data['description'],
                      body=article_data['body'],
                      user_id=user_id)
    try:
        ArticleService.create_article(article)
        return jsonify({'article': article.as_author_dictionary(user_id)}), HTTP_201_CREATED
    except IntegrityError:
        return json_error('Unexpected error', HTTP_422_UNPROCESSABLE_ENTITY)


# update article
@articles_api.put("/<string:slug>")
@jwt_required()
def update_article(slug):
    user_id = get_jwt_identity()
    article = ArticleService.get_article_by_slug(slug)
    if not article:
        return json_error('Article not found', HTTP_404_NOT_FOUND)

    try:
        article_data = update_article_cmd_schema.loads(json_data=json.dumps(request.json))
    except ValidationError as err:
        return json_error(err.messages, HTTP_400_BAD_REQUEST)

    article.slug = article_data.get('slug', article.slug)
    article.title = article_data.get('email', article.title)
    article.description = article_data.get('bio', article.description)
    article.body = article_data.get('image', article.body)

    try:
        article = ArticleService.update_article(article)
        return jsonify({'article': article.as_author_dictionary(user_id)}), HTTP_200_OK
    except IntegrityError:
        return json_error('Unexpected error', HTTP_422_UNPROCESSABLE_ENTITY)


# get_article_by_slug
@articles_api.get("/<string:slug>")
@jwt_required()
def get_article_by_slug(slug):
    user_id = get_jwt_identity()
    article = ArticleService.get_article_by_slug(slug)
    if not article:
        return json_error('Article not found', HTTP_404_NOT_FOUND)
    return jsonify({'article': article.as_author_dictionary(user_id)}), HTTP_200_OK


# get_all
@articles_api.get("/")
@jwt_required()
def get_all():
    articles = ArticleService.get_all_articles()
    articles_dictionary = []
    for article in articles:
        articles_dictionary.append(article.as_dictionary())
    return jsonify({'articles': articles_dictionary}), HTTP_200_OK


# delete_article_by_slug
@articles_api.delete("/<string:slug>")
@jwt_required()
def delete_article_by_slug(slug):
    user_id = get_jwt_identity()
    article = ArticleService.get_article_by_slug(slug)
    if not article:
        return json_error('Article not found', HTTP_404_NOT_FOUND)

    if article.user_id != user_id:
        return json_error('Article owner conflict', HTTP_409_CONFLICT)

    try:
        ArticleService.delete_article(article)
        return jsonify({}), HTTP_204_NO_CONTENT
    except IntegrityError:
        return json_error('Unexpected error', HTTP_422_UNPROCESSABLE_ENTITY)

# def toSlug(title):
#    if title:
#        return title.lower()
