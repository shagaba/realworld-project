import json

from flask import Blueprint, request
from flask.json import jsonify
from flask_jwt_extended import jwt_required, get_jwt_identity
from marshmallow import ValidationError
from sqlalchemy.exc import IntegrityError
from ..constants.http_status_codes import HTTP_200_OK, HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND, \
    HTTP_422_UNPROCESSABLE_ENTITY
from ..dto.user_schema import update_user_cmd_schema
from ..errors import json_error
from ..services.user_service import UserService

user_api = Blueprint('user_api', __name__, url_prefix="/api/user")


# get current user
@user_api.get("/")
@jwt_required()
def get_current_user():
    user_id = get_jwt_identity()
    user = UserService.get_user_by_id(user_id)
    if user:
        return jsonify({'user': user.as_auth_dictionary(get_authorization_bearer_token())}), HTTP_200_OK
    return json_error('User not found', HTTP_404_NOT_FOUND)


# update user
@user_api.put("/")
@jwt_required()
def update_user():
    try:
        user_data = update_user_cmd_schema.loads(json_data=json.dumps(request.json))
    except ValidationError as err:
        return json_error(err.messages, HTTP_400_BAD_REQUEST)

    user_id = get_jwt_identity()
    user = UserService.get_user_by_id(user_id)
    if user is None:
        return json_error('User not found', HTTP_404_NOT_FOUND)

    user.username = user_data.get('username', user.username)
    user.email = user_data.get('email', user.email)
    user.bio = user_data.get('bio', user.bio)
    user.image = user_data.get('image', user.image)
    user.password = user_data.get('password', user.password)

    try:
        user = UserService.update_user(user)
        return jsonify({'user': user.as_auth_dictionary(get_authorization_bearer_token())}), HTTP_200_OK
    except IntegrityError:
        return json_error('Unexpected error', HTTP_422_UNPROCESSABLE_ENTITY)


def get_authorization_bearer_token():
    return request.headers.get('Authorization', '').split()[1]
