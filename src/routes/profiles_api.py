from flask import Blueprint
from flask.json import jsonify
from flask_jwt_extended import get_jwt_identity, jwt_required
from sqlalchemy.exc import IntegrityError
from ..constants.http_status_codes import HTTP_200_OK, HTTP_404_NOT_FOUND, HTTP_422_UNPROCESSABLE_ENTITY
from ..errors import json_error
from ..services.profile_service import ProfileService, UserProfile
from ..services.user_service import UserService

profiles_api = Blueprint('profiles_api', __name__, url_prefix="/api/profiles")


# Get a profile
@profiles_api.get("/<string:followed_user_name>")
@jwt_required()
def get_profile(followed_user_name):
    user_id = get_jwt_identity()
    followed_user = UserService.get_user_by_username(followed_user_name)
    if followed_user is None:
        return json_error('User not found', HTTP_404_NOT_FOUND)

    profile = ProfileService.get_followed_user_profile(followed_user, user_id)
    return jsonify({'profile': profile.as_dictionary()}), HTTP_200_OK


# Follow a user
@profiles_api.post("/<string:followed_user_name>/follow")
@jwt_required()
def follow(followed_user_name):
    user_id = get_jwt_identity()
    followed_user = UserService.get_user_by_username(followed_user_name)
    if followed_user is None:
        return json_error('User not found', HTTP_404_NOT_FOUND)

    if ProfileService.is_followed(followed_user, user_id):
        return jsonify({'profile': UserProfile(followed_user, True).as_dictionary()}), HTTP_200_OK

    try:
        profile = ProfileService.follow_user(followed_user, user_id)
        return jsonify({'profile': profile.as_dictionary()}), HTTP_200_OK
    except IntegrityError:
        return json_error('Unexpected error', HTTP_422_UNPROCESSABLE_ENTITY)


# Unfollow a user
@profiles_api.delete("/<string:followed_user_name>/follow")
@jwt_required()
def unfollow(followed_user_name):
    user_id = get_jwt_identity()
    followed_user = UserService.get_user_by_username(followed_user_name)
    if followed_user is None:
        return json_error('User not found', HTTP_404_NOT_FOUND)

    if not ProfileService.is_followed(followed_user, user_id):
        return jsonify({'profile': UserProfile(followed_user, False).as_dictionary()}), HTTP_200_OK

    try:
        profile = ProfileService.unfollow_user(followed_user, user_id)
        return jsonify({'profile': profile.as_dictionary()}), HTTP_200_OK
    except IntegrityError:
        return json_error('Unexpected error', HTTP_422_UNPROCESSABLE_ENTITY)
