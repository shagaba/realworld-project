import json

from flask import Blueprint, request
from flask.json import jsonify
from flask_jwt_extended import create_access_token
from marshmallow import ValidationError
from sqlalchemy.exc import IntegrityError
from ..constants.http_status_codes import HTTP_200_OK, HTTP_201_CREATED, HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED, \
    HTTP_409_CONFLICT, HTTP_422_UNPROCESSABLE_ENTITY
from ..dto.user_schema import register_user_cmd_schema, authentication_cmd_schema, user_query_schema
from ..errors import json_error
from ..services.identity_service import IdentityService
from ..services.user_service import UserService

users_api = Blueprint('users_api', __name__, url_prefix="/api/users")


# user registration
@users_api.post("/")
def user_registration():
    try:
        user = register_user_cmd_schema.loads(json_data=json.dumps(request.json))
    except ValidationError as err:
        return json_error(err.messages, HTTP_400_BAD_REQUEST)

    if UserService.get_user_by_username(user.username):
        return json_error('Username already exists', HTTP_409_CONFLICT)

    if UserService.get_user_by_email(user.email):
        return json_error('Email already exists', HTTP_409_CONFLICT)

    try:
        UserService.create_user(user)
        access = create_access_token(identity=user.id)
        return jsonify({'user': user.as_auth_dictionary(access)}), HTTP_201_CREATED
    except IntegrityError:
        return json_error('Unexpected error', HTTP_422_UNPROCESSABLE_ENTITY)


# user authentication
@users_api.post("/login")
def user_authentication():
    try:
        request_data = authentication_cmd_schema.loads(json_data=json.dumps(request.json))
    except ValidationError as err:
        return json_error(err.messages, HTTP_400_BAD_REQUEST)

    user = UserService.get_user_by_email(request_data['email'])
    if user:
        is_approved = IdentityService.check_password(user.password, request_data['password'])
        if is_approved:
            access = create_access_token(identity=user.id)
            return jsonify({'user': user.as_auth_dictionary(access)}), HTTP_200_OK

    return json_error('Wrong credentials', HTTP_401_UNAUTHORIZED)
