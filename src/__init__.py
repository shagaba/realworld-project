from flask import Flask
import os
from .extensions import bcrypt, jwtManager, db, migrate
from .routes.user_api import user_api
from .routes.users_api import users_api
from .routes.profiles_api import profiles_api
from .routes.articles_api import articles_api


def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)

    if test_config is None:
        app.config.from_mapping(
            SECRET_KEY=os.environ.get("SECRET_KEY"),
            SQLALCHEMY_DATABASE_URI=os.environ.get("SQLALCHEMY_DB_URI"),
            SQLALCHEMY_TRACK_MODIFICATIONS=False,
            JWT_SECRET_KEY=os.environ.get('JWT_SECRET_KEY'),
        )
    else:
        app.config.from_mapping(test_config)

    bcrypt.init_app(app)
    jwtManager.init_app(app)
    db.init_app(app)
    migrate.init_app(app, db)

    app.register_blueprint(user_api)
    app.register_blueprint(users_api)
    app.register_blueprint(profiles_api)
    app.register_blueprint(articles_api)

    return app
