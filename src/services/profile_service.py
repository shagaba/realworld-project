from sqlalchemy.exc import IntegrityError

from ..dbmodels.profile_db_model import FollowAssociation
from ..extensions import db


class UserProfile:
    def __init__(self, user, following):
        self.user = user
        self.following = following

    def as_dictionary(self):
        return {
            'username': self.user.username,
            'bio': self.user.bio,
            'image': self.user.image,
            'following': self.following,
        }


class ProfileService:

    @staticmethod
    def get_followed_user_profile(followed_user, follower_user_id) -> UserProfile:
        follow_association = FollowAssociation.query.filter_by(followed_user_id=followed_user.id,
                                                               follower_user_id=follower_user_id).one_or_none()
        return UserProfile(user=followed_user, following=follow_association is not None)

    @staticmethod
    def follow_user(followed_user, follower_user_id) -> UserProfile:
        follow_association = FollowAssociation(followed_user_id=followed_user.id, follower_user_id=follower_user_id)
        try:
            db.session.add(follow_association)
            db.session.commit()
            return UserProfile(user=followed_user, following=True)
        except IntegrityError:
            db.session.rollback()
            raise

    @staticmethod
    def unfollow_user(followed_user, follower_user_id) -> UserProfile:
        follow_association = FollowAssociation.query.filter_by(followed_user_id=followed_user.id,
                                                               follower_user_id=follower_user_id).one_or_none()
        try:
            db.session.delete(follow_association)
            db.session.commit()
            return UserProfile(user=followed_user, following=False)
        except IntegrityError:
            db.session.rollback()
            raise

    @staticmethod
    def is_followed(followed_user, follower_user_id) -> bool:
        follow_association = FollowAssociation.query.filter_by(followed_user_id=followed_user.id,
                                                               follower_user_id=follower_user_id).one_or_none()
        return follow_association is not None
