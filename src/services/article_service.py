import datetime
from typing import List

from sqlalchemy.exc import IntegrityError
from ..extensions import db
from ..dbmodels.article_db_model import Article


class ArticleService:

    @staticmethod
    def get_article_by_slug(slug) -> Article:
        return Article.query.filter_by(slug=slug).one_or_none()

    @staticmethod
    def get_all_articles() -> List[Article]:
        return Article.query.all()

    @staticmethod
    def create_article(article) -> Article:
        try:
            db.session.add(article)
            db.session.commit()
            return article
        except IntegrityError:
            db.session.rollback()
            raise

    @staticmethod
    def update_article(article) -> Article:
        article.updated_at = datetime.now()
        try:
            db.session.commit()
            return article
        except IntegrityError:
            db.session.rollback()
            raise

    @staticmethod
    def delete_article(article) -> None:
        try:
            db.session.delete(article)
            db.session.commit()
        except IntegrityError:
            db.session.rollback()
            raise
