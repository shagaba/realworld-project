from ..extensions import bcrypt


class IdentityService:

    @staticmethod
    def check_password(password_hash, password) -> bool:
        return bcrypt.check_password_hash(password_hash, password)

    @staticmethod
    def generate_password_hash(password) -> bytes:
        return bcrypt.generate_password_hash(password)
