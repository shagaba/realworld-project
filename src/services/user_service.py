from sqlalchemy.exc import IntegrityError

from ..dbmodels.user_db_model import User
from ..extensions import db
from datetime import datetime


class UserService:

    @staticmethod
    def get_user_by_username(username) -> User:
        return User.query.filter_by(username=username).one_or_none()

    @staticmethod
    def get_user_by_id(user_id) -> User:
        return User.query.filter_by(id=user_id).one_or_none()

    @staticmethod
    def get_user_by_email(email) -> User:
        return User.query.filter_by(email=email).one_or_none()

    @staticmethod
    def create_user(user) -> User:
        try:
            db.session.add(user)
            db.session.commit()
            return user
        except IntegrityError:
            db.session.rollback()
            raise

    @staticmethod
    def update_user(user) -> User:
        user.updated_at = datetime.now()
        try:
            db.session.commit()
            return user
        except IntegrityError:
            db.session.rollback()
            raise

