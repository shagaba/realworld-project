import json

from marshmallow import Schema, fields, ValidationError, validate, post_load, pre_load

from ..dbmodels.user_db_model import User
from ..services.identity_service import IdentityService


class _RegisterUserSchema(Schema):
    username = fields.String(required=True)
    email = fields.String(required=True, validate=validate.Email(error="Not a valid email address"))
    password = fields.String(required=True, validate=[validate.Length(min=6, max=36)])


class RegisterUserCmdSchema(Schema):
    user = fields.Nested(_RegisterUserSchema)

    @pre_load
    def process_input(self, data, **kwargs):
        user_data = data.get('user', None)
        if user_data is None:
            raise ValidationError('User field is missing')
        user_data['email'] = user_data['email'].lower().strip()
        return data

    @post_load
    def make_object(self, data, **kwargs):
        user_data = data.get('user')
        password_hash = IdentityService.generate_password_hash(user_data['password'])
        return User(username=user_data['username'], email=user_data['email'], password=password_hash)


class _AuthenticateSchema(Schema):
    email = fields.String(required=True, validate=validate.Email(error="Not a valid email address"))
    password = fields.String(required=True, validate=[validate.Length(min=6, max=36)])


class AuthenticationCmdSchema(Schema):
    user = fields.Nested(_AuthenticateSchema)

    @pre_load
    def process_input(self, data, **kwargs):
        user_data = data.get('user', None)
        if user_data is None:
            raise ValidationError('User field is missing')
        user_data['email'] = user_data['email'].lower().strip()
        return data

    @post_load
    def make_object(self, data, **kwargs):
        return data.get('user')


class _UpdateUserSchema(Schema):
    username = fields.String(required=False)
    email = fields.String(required=False, validate=validate.Email(error="Not a valid email address"))
    password = fields.String(required=False, validate=[validate.Length(min=6, max=36)])
    bio = fields.String(required=False)
    image = fields.String(required=False)


class UpdateUserCmdSchema(Schema):
    user = fields.Nested(_UpdateUserSchema)

    @pre_load
    def process_input(self, data, **kwargs):
        user_data = data.get('user', None)
        if user_data is None:
            raise ValidationError('User field is missing')
        if user_data['email']:
            user_data['email'] = user_data['email'].lower().strip()
        return data

    @post_load
    def make_object(self, data, **kwargs):
        user_data = data.get('user')
        if user_data['password']:
            user_data['password'] = IdentityService.generate_password_hash(user_data['password'])
        return user_data


class UserQuerySchema(Schema):
    username = fields.String(required=False)
    email = fields.String(required=False)
    bio = fields.String(required=False)
    image = fields.String(required=False)
    token = fields.String(required=False)
    createdAt = fields.DateTime(attribute='created_at')
    updatedAt = fields.DateTime(attribute='updated_at')


register_user_cmd_schema = RegisterUserCmdSchema()
authentication_cmd_schema = AuthenticationCmdSchema()
update_user_cmd_schema = UpdateUserCmdSchema()
user_query_schema = UserQuerySchema()
