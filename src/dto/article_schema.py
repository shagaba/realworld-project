import json

from marshmallow import Schema, fields, ValidationError, validate, post_load, pre_load
from slugify import slugify

from ..dbmodels.user_db_model import User
from ..services.identity_service import IdentityService


class _CreateArticleSchema(Schema):
    title = fields.String(required=True)
    description = fields.String(required=True)
    body = fields.String(required=True)


class CreateArticleCmdSchema(Schema):
    article = fields.Nested(_CreateArticleSchema)

    @pre_load
    def process_input(self, data, **kwargs):
        article_data = data.get('article', None)
        if article_data is None:
            raise ValidationError('Article field is missing')
        article_data['slug'] = slugify(article_data['title'])
        return article_data

    @post_load
    def make_object(self, data, **kwargs):
        return data.get('article')


class _UpdateArticleSchema(Schema):
    title = fields.String(required=False)
    description = fields.String(required=False)
    body = fields.String(required=False)


class UpdateArticleCmdSchema(Schema):
    article = fields.Nested(_UpdateArticleSchema)

    @pre_load
    def process_input(self, data, **kwargs):
        article_data = data.get('article', None)
        if article_data is None:
            raise ValidationError('Article field is missing')
        if article_data['title']:
            article_data['slug'] = slugify(article_data['title'])
        return article_data

    @post_load
    def make_object(self, data, **kwargs):
        return data.get('article')


create_article_cmd_schema = CreateArticleCmdSchema()
update_article_cmd_schema = UpdateArticleCmdSchema()
